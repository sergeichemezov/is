/**
 * Created by sergo on 12.09.2016.
 */
public class Calculate {

    public static double sub(double x, double y){
        return x-y;
    }

    public static double div_double(double x, double y){
        return x/y;
    }

    public static double div(int x, int y){
        return x/y;
    }


    public static double mult(double x, double y){
        return x*y;
    }

    public static double add(double x, double y){
        return x+y;
    }

    public static double mod(int x, int y){
        return x%y;
    }

}
