/**
 * Created by sergo on 12.09.2016.
 */
public class Demo {
    public static void main(String[] args) {

        System.out.println("Результат вещественного деления деления:");
        System.out.println(Calculate.div_double(10,5));
        System.out.println("Результат целочисленного деления деления:");
        System.out.println(Calculate.div(11,5));
        System.out.println("Результат вычитания:");
        System.out.println(Calculate.sub(10,5));
        System.out.println("Результат умножения:");
        System.out.println(Calculate.mult(10,5));
        System.out.println("Результат сложения:");
        System.out.println(Calculate.add(10,5));
        System.out.println("Результат выделения остатка от деления:");
        System.out.println(Calculate.mod(11,5));
    }
}